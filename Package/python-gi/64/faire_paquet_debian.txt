# Outil à installer :
# sudo apt-get install debhelper cdbs lintian build-essential fakeroot devscripts pbuilder dh-make debootstrap
#
# https://www.debian-fr.org/t/extraire-paquet-deb/31870
#
# Pour extraire le paquet :
#
# dpkg-deb -x paquet.deb repertoire -> extrait l'arborescence
# dpkg-deb -e paquet.deb -> extrait le répertoire DEBIAN contenant les différents fichiers postinst, control, etc
#
# Pour assembler le paquet :
#
# dpkg-deb -b repertoire paquet.deb
#

# Méthode paquet origine

version=3.36.0-3_amd64
nom_paquet=python-gi
ext_paquet=deb

mkdir ${nom_paquet}
dpkg-deb -x ${nom_paquet}_${version}.${ext_paquet} ${nom_paquet}
dpkg-deb -e ${nom_paquet}_${version}.${ext_paquet} ${nom_paquet}/DEBIAN

# Modifier le fichier DEBIAN/control en mettant cela :

Changer le numéro de version 3.36.0-3 du paquet en :
Version: 3.43.0-1
car conflit avec ce paquet : libgirepository-1.0-1:i386 (1.70.0-2) breaks python-gi (<< 3.42.0-1+b1) and is installed.

# Méthode assemblage

dpkg-deb -b ${nom_paquet} ${nom_paquet}_${version}_mod_emma.${ext_paquet}



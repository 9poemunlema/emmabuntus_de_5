#!/bin/bash


# emmabuntus_tools.sh --
#
#   This file permits to display a Tools panel for the Emmabuntüs Distrib.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

########################################################################################################

. "$HOME/.config/user-dirs.dirs"


clear


###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0


nom_distribution="Emmabuntus Debian Edition 5"
nom_titre_window="Emmabuntüs - Tools"

repertoire_script=/usr/bin

source /usr/bin/emmabuntus_found_theme.sh
highlight_color=$(FOUND_HIGHLIGHT_COLOR)



# Initilisation des variables de la fenêtre de dialogue
HEIGHT_WINDOW=680
TAILLE_ICONE=28
TOOL_VISIBLE_LIVE=true
TOOL_VISIBLE_INSTALL=true
TOOL_INVISIBLE=false

if [[ $(cat /proc/cmdline | grep -i boot=live) ]]
then
    TOOL_VISIBLE_LIVE=false
else
    TOOL_VISIBLE_INSTALL=false
fi


export EMMABUNTUS_TOOLS='<window title="'$(eval_gettext 'Emmabuntüs - Tools')'" icon-name="gtk-info" width_request="600"  height_request="'${HEIGHT_WINDOW}'" resizable="false">
 <vbox spacing="5" >
 <text use-markup="true" wrap="false" justify="2">
 <input>echo "\n\<b>\<span color=\"'${highlight_color}'\">'$(eval_gettext 'These applications help you manage your system.')'\</span>\</b>" | sed "s%\\\%%g"</input>
 </text>

 <frame>

 <text use-markup="true" wrap="false" xalign="0" justify="3">
 <input>echo "\<b>\<u>"'$(eval_gettext 'Live')'"\</u>\</b>" | sed "s%\\\%%g"</input>
 </text>

 <hbox spacing="0" space-expand="true" space-fill="false">


    <button image-position="0" >
    <input file>/usr/share/icons/ventoy.png</input>
    <width>'$TAILLE_ICONE'</width>
    <label>  Ventoy  </label>
    <action>'${repertoire_script}'/emmabuntus_tools_func.sh Ventoy</action>
    </button>

    <button image-position="0" >
    <input file>/usr/share/icons/revival-blue/apps/scalable/usb-creator.svg</input>
    <width>'$TAILLE_ICONE'</width>
    <label>USB-Creator</label>
    <action>'${repertoire_script}'/emmabuntus_tools_func.sh USB-Creator</action>
    </button>

    <button image-position="0">
    <input file>/usr/share/icons/revival-blue/apps/scalable/usb-creator.svg</input>
    <width>'$TAILLE_ICONE'</width>
    <label>USB-Formatter</label>
    <action>'${repertoire_script}'/emmabuntus_tools_func.sh USB-Formatter</action>
    </button>

 </hbox>

 <hseparator space-expand="true" space-fill="true"></hseparator>

 <text use-markup="true" wrap="false" xalign="0" justify="3">
 <input>echo "\<b>\<u>"'$(eval_gettext 'Maintenance')'"\</u>\</b>" | sed "s%\\\%%g"</input>
 </text>

 <hbox spacing="10" homogenous="true" space-expand="true" space-fill="false" xalign="2">

    <button image-position="0">
    <input file>/usr/share/icons/revival-blue/apps/scalable/bleachbit.svg</input>
    <width>'$TAILLE_ICONE'</width>
    <label>"    BleachBit    "</label>
    <action>'${repertoire_script}'/emmabuntus_tools_func.sh BleachBit</action>
    </button>

    <button image-position="0">
    <input file>/usr/share/icons/xboot-repair.png</input>
    <width>'$TAILLE_ICONE'</width>
    <label>"    Boot-Repair   "</label>
    <action>'${repertoire_script}'/emmabuntus_tools_func.sh Boot-Repair</action>
    </button>

    <button image-position="0" visible="'${TOOL_VISIBLE_INSTALL}'">
    <input file>/usr/share/icons/xos-uninstaller.png</input>
    <width>'$TAILLE_ICONE'</width>
    <label>" OS-Uninstaller  "</label>
    <action>'${repertoire_script}'/emmabuntus_tools_func.sh OS-Uninstaller</action>
    </button>

    <button image-position="0" visible="'${TOOL_VISIBLE_LIVE}'">
    <input file>/usr/share/icons/removed_languages.svg</input>
    <width>'$TAILLE_ICONE'</width>
    <label>"Remove languages"</label>
    <action>'${repertoire_script}'/emmabuntus_tools_func.sh Remove_languages</action>
    </button>


 </hbox>

 <hbox spacing="10" homogenous="true" space-expand="true" space-fill="false" xalign="2">

    <button image-position="0">
    <input file>/usr/share/icons/revival-blue/devices/scalable/gnome-disks.svg</input>
    <width>'$TAILLE_ICONE'</width>
    <label>"    Disk Info    "</label>
    <action>'${repertoire_script}'/emmabuntus_tools_func.sh Disk-Info</action>
    </button>

    <button image-position="0" visible="'${TOOL_VISIBLE_LIVE}'">
    <input file>/usr/share/icons/revival-blue/apps/scalable/timeshift.svg</input>
    <width>'$TAILLE_ICONE'</width>
    <label>"       TimeShift      "</label>

    <action>'${repertoire_script}'/emmabuntus_tools_func.sh TimeShift</action>
    </button>

 </hbox>

 <hseparator space-expand="true" space-fill="true"></hseparator>

 <text use-markup="true" wrap="false" xalign="0" justify="3">
 <input>echo "\<b>\<u>"'$(eval_gettext 'Configuration')'"\</u>\</b>" | sed "s%\\\%%g"</input>
 </text>

 <hbox spacing="10" homogenous="true" space-expand="true" space-fill="false" xalign="2">

    <button image-position="0">
    <input file>/usr/share/icons/revival-blue/apps/scalable/cairo-dock.svg</input>
    <width>'$TAILLE_ICONE'</width>
    <label>"  Cairo Dock  "</label>
    <action>'${repertoire_script}'/emmabuntus_tools_func.sh Cairo-Dock</action>
    </button>

    <button image-position="0">
    <input file>/usr/share/icons/revival-blue/apps/scalable/multimedia-volume-control.svg</input>
    <width>'$TAILLE_ICONE'</width>
    <label>" System Sound "</label>
    <action>'${repertoire_script}'/emmabuntus_tools_func.sh System-Sound</action>
    </button>

    <button image-position="0">
    <input file>/usr/share/icons/revival-blue/apps/scalable/multimedia-volume-control.svg</input>
    <width>'$TAILLE_ICONE'</width>
    <label>"  ALSA Mixer   "</label>
    <action>'${repertoire_script}'/emmabuntus_tools_func.sh ALSA-Mixer</action>
    </button>

 </hbox>

 <hbox spacing="10" homogenous="true" space-expand="true" space-fill="false" xalign="2">

  <button image-position="0" visible="'${TOOL_VISIBLE_LIVE}'">
    <input file>/usr/share/icons/revival-blue/apps/scalable/screenruler.svg</input>
    <width>'$TAILLE_ICONE'</width>
    <label>"  Fix Tearing  "</label>
    <action>'${repertoire_script}'/emmabuntus_tools_func.sh Fix_tearing</action>
    </button>

    <button image-position="0" visible="'${TOOL_VISIBLE_LIVE}'">
    <input file>/usr/share/icons/revival-blue/places/scalable/preferences-desktop-locale.svg</input>
    <width>'$TAILLE_ICONE'</width>
    <label>"  Locale  "</label>
    <action>'${repertoire_script}'/emmabuntus_tools_func.sh Locale</action>
    </button>

    <button image-position="0" visible="'${TOOL_VISIBLE_LIVE}'">
    <input file>/usr/share/icons/install_no_freesoftware.png</input>
    <width>'$TAILLE_ICONE'</width>
    <label>Install no Free</label>
    <action>'${repertoire_script}'/emmabuntus_tools_func.sh Install-no-Free</action>
    </button>

 </hbox>

 <hseparator space-expand="true" space-fill="true"></hseparator>

 <text use-markup="true" wrap="false" xalign="0" justify="3">
 <input>echo "\<b>\<u>"'$(eval_gettext 'Printing')'"\</u>\</b>" | sed "s%\\\%%g"</input>
 </text>

 <hbox spacing="0" space-expand="true" space-fill="false">

    <button image-position="0">
    <input file>/usr/share/icons/revival-blue/devices/scalable/printer.svg</input>
    <width>'$TAILLE_ICONE'</width>
    <label>" Config "</label>
    <action>'${repertoire_script}'/emmabuntus_tools_func.sh Config-Printer</action>
    </button>

    <button image-position="0">
    <input file>/usr/share/hplip/data/images/128x128/hp_logo.png</input>
    <width>'$TAILLE_ICONE'</width>
    <label>" HPLip "</label>
    <action>'${repertoire_script}'/emmabuntus_tools_func.sh HPLip</action>
    </button>

    <button image-position="0">
    <input file>/usr/share/icons/revival-blue/devices/scalable/printer.svg</input>
    <width>'$TAILLE_ICONE'</width>
    <label>" Brother "</label>
    <action>'${repertoire_script}'/emmabuntus_tools_func.sh Brother</action>
    </button>

    <button image-position="0">
    <input file>/usr/share/icons/turboprint-icon.png</input>
    <width>'$TAILLE_ICONE'</width>
    <label>" TurboPrint "</label>
    <action>'${repertoire_script}'/emmabuntus_tools_func.sh TurboPrint</action>
    </button>

 </hbox>

 <hseparator space-expand="true" space-fill="true"></hseparator>

 <text use-markup="true" wrap="false" xalign="0" justify="3">
 <input>echo "\<b>\<u>"'$(eval_gettext 'Utilities')'"\</u>\</b>" | sed "s%\\\%%g"</input>
 </text>

 <hbox spacing="0" space-expand="true" space-fill="false">

    <button image-position="0">
    <input file>/usr/share/icons/revival-blue/devices/scalable/computer.svg</input>
    <width>'$TAILLE_ICONE'</width>
    <label>System Info</label>
    <action>'${repertoire_script}'/emmabuntus_tools_func.sh System-Info</action>
    </button>

    <button image-position="0">
    <input file>/usr/share/hardinfo/pixmaps/logo.png</input>
    <width>'$TAILLE_ICONE'</width>
    <label>System Profiler</label>
    <action>'${repertoire_script}'/emmabuntus_tools_func.sh System-Profiler</action>
    </button>

    <button image-position="0" visible="'${TOOL_VISIBLE_LIVE}'">
    <input file>/usr/share/icons/flatpak.png</input>
    <width>'$TAILLE_ICONE'</width>
    <label>Flatpak</label>
    <action>'${repertoire_script}'/emmabuntus_tools_func.sh Flatpak</action>
    </button>

    <button image-position="0">
    <input file>/usr/share/icons/lxkeymap.png</input>
    <width>'$TAILLE_ICONE'</width>
    <label>Lxkeymap</label>
    <action>'${repertoire_script}'/emmabuntus_tools_func.sh Lxkeymap</action>
    </button>

    <button image-position="0">
    <input file>/usr/share/icons/revival-blue/devices/scalable/display.svg</input>
    <width>'$TAILLE_ICONE'</width>
    <label>ARandR</label>
    <action>'${repertoire_script}'/emmabuntus_tools_func.sh ARandR</action>
    </button>

 </hbox>

 </frame>


    <hbox spacing="10" space-expand="false" space-fill="true">
    <button is-focus="true">
    <input file icon="gtk-close"></input>
    <label>"'$(eval_gettext 'Close')'"</label>
    <action>pgrep -f emmabuntus_tools | xargs kill -9 > /dev/null</action>
    <action>exit:exit</action>
    </button>
    </hbox>


 </vbox>
 </window>'


    MENU_DIALOG="$(gtkdialog --center --program=EMMABUNTUS_TOOLS)"




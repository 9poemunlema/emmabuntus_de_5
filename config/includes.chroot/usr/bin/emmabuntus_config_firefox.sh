#!/bin/bash

# Emmabuntus_config_firefox.sh --
#
#   This file permits to initialize a correct system language on Firefox on Emmabuntüs.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

. "$HOME/.config/user-dirs.dirs"

clear



nom_distribution="Emmabuntus Debian Edition 5"

repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
fichier_init_config=${repertoire_emmabuntus}/config_language_firefox.txt
config_file_firefox=~/.mozilla/firefox/tsgexiex.default/prefs.js

#Modification chaine : user_pref("intl.accept_languages", "fr-ES, fr, en-US, en");

langue=""
langue_sup=""


if [[ ! ( -f $fichier_init_config ) ]]
then


langue=`echo $LANG | cut -d_ -f 1 `

langue_sup=`echo $LANG | cut -d. -f 1 `
langue_sup=`echo $langue_sup | cut -d_ -f 2 `
langue_sup="${langue}-${langue_sup}"


echo "langue=${langue} & langue_sup=${langue_sup}"


if [[ ${langue} = ""  || ${langue_sup} = "" ]]
then

    echo "Config Emmabuntüs Firefox language : nul parameters language & language sup " >> ${fichier_init_config}

    exit 10

else

    if [[ ${langue} == "en" && ${langue_sup} == "en-US" ]]
    then

        sed s/"^user_pref(\"intl.accept_languages\",[^;]*;"/"user_pref(\"intl.accept_languages\", \"en-US, en\");"/ ${config_file_firefox} > ${config_file_firefox}.tmp

        cp ${config_file_firefox}.tmp ${config_file_firefox}
        rm ${config_file_firefox}.tmp

        echo "Config Emmabuntüs Firefox language :  en-US & en" >> ${fichier_init_config}

    else

        sed s/"^user_pref(\"intl.accept_languages\",[^;]*;"/"user_pref(\"intl.accept_languages\", \"${langue_sup}, ${langue}, en-US, en\");"/ ${config_file_firefox} > ${config_file_firefox}.tmp

        cp ${config_file_firefox}.tmp ${config_file_firefox}
        rm ${config_file_firefox}.tmp

        echo "Config Emmabuntüs Firefox language : ${langue_sup} & ${langue}" >> ${fichier_init_config}
    fi

fi

if [[ ${langue} == "fr" || ${langue} == "en" || ${langue} == "es" ]]
then

    sed s/"^user_pref(\"extensions.jid1-zmgYgiQPXJtjNA@jetpack.lang[^;]*;"/"user_pref(\"extensions.jid1-zmgYgiQPXJtjNA@jetpack.lang\", \"${langue}\");"/ ${config_file_firefox} > ${config_file_firefox}.tmp

    cp ${config_file_firefox}.tmp ${config_file_firefox}
    rm ${config_file_firefox}.tmp

    echo "Config Emmabuntüs Lilo Drop : ${langue}" >> ${fichier_init_config}

else

    sed s/"^user_pref(\"extensions.jid1-zmgYgiQPXJtjNA@jetpack.lang[^;]*;"/"user_pref(\"extensions.jid1-zmgYgiQPXJtjNA@jetpack.lang\", \"en\");"/ ${config_file_firefox} > ${config_file_firefox}.tmp

    cp ${config_file_firefox}.tmp ${config_file_firefox}
    rm ${config_file_firefox}.tmp

    echo "Config Emmabuntüs Lilo Drop : en" >> ${fichier_init_config}
fi

fi

exit 0

#! /bin/bash

# calamares_oem_switch.sh --
#
#   This file permits to switch OEM postinstall for the Emmabuntus Distrib.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

. "$HOME/.config/user-dirs.dirs"

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0


repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
fichier_init=${repertoire_emmabuntus}/postinstall_calamares.txt

dir_config_desktop=${XDG_DESKTOP_DIR}
dir_config_autostart="/home/oem/.config/autostart"
dir_config_xfce="/home/oem/.config/xfce4/xfconf/xfce-perchannel-xml"
dir_config_lxqt="/home/oem/.config/lxqt"
dir_config_pcmanfm_qt="/home/oem/.config/pcmanfm-qt/lxqt"
fichier_install_oem="/etc/skel/.config/emmabuntus/install_oem.txt"
fichier_lanceur_grub_oem="/etc/skel/.config/autostart/calamares_oem_preinstall_grub.desktop"
dir_config_oem_id="/var/log/installer"
file_config_oem_id="oem-id"

copie_env_utilisateur="false"
copie_postinstall_oem="false"
OEM_ID=""

bureau=${XDG_DESKTOP_DIR}

if [[ ($USER == "oem") ]] ; then

DATE=`date +"%d:%m:%Y - %H:%M:%S"`

source /usr/bin/emmabuntus_found_theme.sh
highlight_color=$(FOUND_HIGHLIGHT_COLOR)

message_accueil="\n\
$(eval_gettext 'You are about to finalize the OEM installation before shipping it to the final user(s).')\n\
\n\
$(eval_gettext 'After this window activation, you won\047t be able to use the OEM environment at the next startup.')\n\
\n\
\<span color=\'${highlight_color}\'>$(eval_gettext 'Note:')\</span> $(eval_gettext 'if you wish to define the current OEM user environment as the one by default')\n\
$(eval_gettext 'for the final users, please select either the first or both options below:')"


    export WINDOW_DIALOG_WELCOME='<window title="'$(eval_gettext 'Finalize the OEM installation')'" icon-name="gtk-info" resizable="false">
    <vbox spacing="0">

    <text use-markup="true" wrap="false" xalign="0" justify="3">
    <input>echo "'${message_accueil}'" | sed "s%\\\%%g"</input>
    </text>

    <hseparator space-expand="true" space-fill="true"></hseparator>

    <hbox>
        <text>
        <label>OEM ID : </label>
        </text>
        <entry is-focus="true" space-fill="false" width_request="200" tooltip-text="'$(eval_gettext 'Please enter a unique name for this batch of systems using only alphanumeric characters. This name will be saved on the installed system on the file')' ${dir_config_oem_id}/${file_config_oem_id}">
        <default>""</default>
        <width>10</width>
        <input>case $OEM_ID in ""|*[\w]*) echo "";; *) echo $OEM_ID ;; esac</input>
        <variable>OEM_ID</variable>
        <action signal="focus-out-event">refresh:OEM_ID</action>
        <action signal="enter-notify-event">refresh:OEM_ID</action>
        </entry>
        <text space-expand="true" space-fill="true" width_request="450">
        <label>" "</label>
        </text>
    </hbox>

    <hseparator space-expand="true" space-fill="true"></hseparator>

    <checkbox height_request="40" active="'${copie_env_utilisateur}'" tooltip-text="'$(eval_gettext 'If you copy only the OEM environment, the final user account creation will launch the post-installation process, allowing the user to define it\047s own configuration (defining dock usage level, enabling applications like RedShifts, etc...)')'">
      <variable>copie_env_utilisateur</variable>
      <label>"'$(eval_gettext 'Copy the OEM environment as reference for the final user')'"</label>
      <action>if true enable:copie_postinstall_oem</action>
      <action>if false disable:copie_postinstall_oem</action>
      <action signal="enter-notify-event">refresh:OEM_ID</action>
    </checkbox>

    <checkbox height_request="40" active="'${copie_postinstall_oem}'" sensitive="'${copie_env_utilisateur}'" tooltip-text="'$(eval_gettext 'By copying also the OEM post-install configuration, this step will be skipped after the account creation of the final user, who will then inherits of the entire OEM configuration')'">
      <variable>copie_postinstall_oem</variable>
      <label>"'$(eval_gettext 'Copy the OEM post-install configuration for the final user(s)')'"</label>
      <action signal="enter-notify-event">refresh:OEM_ID</action>
    </checkbox>

    <hseparator space-expand="true" space-fill="true"></hseparator>

    <hbox spacing="10" space-expand="false" space-fill="false">

    <button use-stock="true">
    <label>"'$(eval_gettext 'Cancel')' "</label>
    <input file stock="gtk-cancel"></input>
    <action signal="enter-notify-event">refresh:OEM_ID</action>
    <action>exit:Cancel</action>
    </button>

    <button>
    <label>"'$(eval_gettext 'Finalize')' "</label>
    <input file stock="gtk-ok"></input>
    <action signal="enter-notify-event">refresh:OEM_ID</action>
    <action>exit:OK</action>
    </button>

    </hbox>

    </vbox>
    </window>'

    MENU_DIALOG_WELCOME="$(gtkdialog --center --program=WINDOW_DIALOG_WELCOME)"
    eval ${MENU_DIALOG_WELCOME}
    echo "MENU_DIALOG_WELCOME=${MENU_DIALOG_WELCOME}"

        if [ ${EXIT} == "OK" ] ; then

            echo "Post-Installation OEM activée " >> ${fichier_init}
            echo "DATE = $DATE" >> ${fichier_init}

            ################################################################################
            # Partie active du script
            ################################################################################

            if [[ ${copie_env_utilisateur} == "true" ]] ; then
                /usr/bin/copy_env_user_system_exec.sh ${copie_env_utilisateur} ${copie_postinstall_oem}
            fi

            # Création du fichier autostart
            tee ${dir_config_autostart}/calamares_oem_postinstall.desktop > /dev/null <<EOT
[Desktop Entry]
Version=1.0
Encoding=UTF-8
Name=Finalization Calamares OEM
GenericName=Finalization Calamares OEM
Name[de]=Finalisierung Calamares OEM
Name[es]=Finalización OEM de Calamares
Name[fr]=Finalisation Calamares OEM
Name[it]=Finalizzazione OEM Calamares
Name[pt]=Finalização OEM Calamares
Comment=Calamares - Finalization of the OEM mode installation for Emmabuntüs Debian Edition
Comment[de]=Calamares - Abschluss der Installation im OEM-Modus für Emmabuntüs Debian Edition
Comment[es]=Calamares - Finalización de la instalación en modo OEM para Emmabuntüs Debian Edition
Comment[fr]=Calamares - Finalisation de l'installation mode OEM pour Emmabuntüs Debian Edition
Comment[it]=Calamares - Completamento dell'installazione in modalità OEM per Emmabuntüs Debian Edition
Comment[pt]=Calamares - Finalização da instalação do modo OEM para Emmabuntüs Debian Edition
Exec=/usr/bin/calamares_oem_postinstall_exec.sh
Terminal=false
Type=Application
Categories=
EOT

            # Suppression du lanceur calamares OEM par le GRUB
            if [[ -f ${fichier_lanceur_grub_oem} ]] ; then
                sudo rm ${fichier_lanceur_grub_oem}
            fi

            # Suppression du lanceur calamares
            if [[ -f ${dir_config_desktop}/install-debian.desktop ]] ; then
                rm ${dir_config_desktop}/install-debian.desktop
            fi

            # Remommage du lanceur d'Emmabuntüs
            if [[ -f ${dir_config_autostart}/start_emmabuntus.desktop ]] ; then
                mv ${dir_config_autostart}/start_emmabuntus.desktop ${dir_config_autostart}/start_emmabuntus.desktop.bak
            fi

            # Remommage du fichier de config des raccourcis sous Xfce et LXQt
            if [[ -f ${dir_config_xfce}/xfce4-keyboard-shortcuts.xml ]] ; then
                mv ${dir_config_xfce}/xfce4-keyboard-shortcuts.xml ${dir_config_xfce}/xfce4-keyboard-shortcuts.xml.bak
            fi

            if [[ -f ${dir_config_lxqt}/globalkeyshortcuts.conf ]] ; then
                mv ${dir_config_lxqt}/globalkeyshortcuts.conf ${dir_config_lxqt}/globalkeyshortcuts.conf.bak
            fi

            # Désactivation clic droit sur le bureau
            if [[ -f ${dir_config_pcmanfm_qt}/settings.conf ]] ; then
                sed s/"ShowWmMenu=false"/"ShowWmMenu=true"/ ${dir_config_pcmanfm_qt}/settings.conf > ${dir_config_pcmanfm_qt}/settings.conf.tmp
                cp ${dir_config_pcmanfm_qt}/settings.conf.tmp ${dir_config_pcmanfm_qt}/settings.conf
                rm ${dir_config_pcmanfm_qt}/settings.conf.tmp
            fi

            # Tag installation OEM
            echo "Installation OEM" >> ${fichier_install_oem}
            echo "DATE=$DATE" | sudo tee -a ${fichier_install_oem}
            echo "OEM_ID=${OEM_ID}" | sudo tee -a ${fichier_install_oem}
            sudo chmod a+r ${fichier_install_oem}


            if [[ ! -d ${dir_config_oem_id} ]] ; then
                sudo mkdir ${dir_config_oem_id}
                sudo chmod -R a+rwX ${dir_config_oem_id}
            fi

            echo "${OEM_ID}" | sudo tee ${dir_config_oem_id}/${file_config_oem_id}
            sudo chmod a+r ${dir_config_oem_id}/${file_config_oem_id}

            export WINDOW_DIALOG_END='<window title="" icon-name="gtk-info" resizable="false">
            <vbox spacing="0">

            <text use-markup="true" wrap="false" xalign="0" justify="3">
            <input>echo "\n'$(eval_gettext 'The computer is ready for shipment')',\n'$(eval_gettext 'and it will stop.')'" | sed "s%\\\%%g"</input>
            </text>

            <hseparator space-expand="true" space-fill="true"></hseparator>

            <hbox spacing="10" space-expand="false" space-fill="false">

            <button is-focus="true">
            <input file icon="gtk-close"></input>
            <label>"'$(eval_gettext 'Close') '"</label>
            <input file stock="gtk-ok"></input>
            <action>exit:OK</action>
            </button>

            </hbox>

            </vbox>
            </window>'

            MENU_DIALOG_WELCOME="$(gtkdialog --center --program=WINDOW_DIALOG_END)"

            systemctl -i poweroff

        else

            echo " Post-Installation OEM annulée " >> ${fichier_init}
            echo "DATE = $DATE" >> ${fichier_init}

            exit 2

        fi

else

    exit 1

fi




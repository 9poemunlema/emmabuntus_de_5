#! /bin/bash


# emmabuntus_choose_keyboard.sh --
#
#   This file permits to choose Keyboard for the Emmabuntüs Distrib.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


########################################################################################################

. "$HOME/.config/user-dirs.dirs"

clear

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0


nom_distribution="Emmabuntus Debian Edition 5"
repertoire_emmabuntus=~/.config/emmabuntus
fichier_init_config=${repertoire_emmabuntus}/init_config_choose_keyboard.txt
delai_fenetre=20
delai_fenetre_selection=120
delai_fenetre_progression=1200 ; # temporisation de 20 minutes
choix=""
mode_keyboard="??"
utilisateur=emmabuntus

bureau=${XDG_DESKTOP_DIR}

source /usr/bin/emmabuntus_found_theme.sh
highlight_color=$(FOUND_HIGHLIGHT_COLOR)

if [[ ! ( -f $fichier_init_config ) ]]
then


mode_keyboard=`setxkbmap -v | awk -F "+" '/symbols/ {print $2}'`

message_demarrage="\n\
$(eval_gettext 'Currently your keyboard map is:') \<span color=\'"'red\'"'>${mode_keyboard}\</span>        "


    export WINDOW_DIALOG_KEYBOARD='<window title="'$(eval_gettext 'Keyboard configuration')'" icon-name="gtk-dialog-question" resizable="false">
    <vbox spacing="0">

    <text use-markup="true" wrap="false" xalign="0" justify="3">
    <input>echo "'${message_demarrage}'" | sed "s%\\\%%g"</input>
    </text>

    <hbox spacing="10" space-expand="false" space-fill="false">
    <button can-default="true" has-default="true" use-stock="true" is-focus="true">
    <label>"'$(eval_gettext ' Switch ')'"</label>
    <input file stock="gtk-cancel"></input>
    <action>exit:OK</action>

    </button>
    <button>
    <label>"'$(eval_gettext ' Keep ')'"</label>
    <input file stock="gtk-ok"></input>
    <action>exit:Cancel</action>
    </button>
    </hbox>

    </vbox>
    </window>'

    MENU_DIALOG_KEYBOARD="$(gtkdialog --center --program=WINDOW_DIALOG_KEYBOARD)"

    eval ${MENU_DIALOG_KEYBOARD}
    echo "MENU_DIALOG_KEYBOARD=${MENU_DIALOG_KEYBOARD}"


    if  [ ${EXIT} == "OK" ]
    then
        lxkeymap
   fi


fi



exit 0

#! /bin/bash

# install_depot_emmabuntus.sh --
#
#   This file permits to install Emmabuntus Repository.
#
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on XFCE Debian 10.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################



clear

########################################################################################################
# Paramètres de configuration de la distribution
########################################################################################################

nom_distribution="Emmabuntus Debian Edition 5"
base_distribution="bookworm"



# Installation cle pour medibuntu free non-free codec
wget -O - http://download.videolan.org/pub/debian/videolan-apt.asc | apt-key add -










